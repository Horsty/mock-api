"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var articles_1 = require("./api/articles");
var moovies_1 = require("./api/moovies");
var persons_1 = require("./api/persons");
var Router = /** @class */ (function () {
    function Router() {
        var _this = this;
        this.routing = function () {
            _this.router.use(Router.URL_API_ARTICLES, new articles_1.ArticleIndex().getRouter());
            _this.router.use(Router.URL_API_MOOVIES, new moovies_1.MoovieIndex().getRouter());
            _this.router.use(Router.URL_API_PERSONS, new persons_1.PersonIndex().getRouter());
        };
        this.getRouter = function () {
            return _this.router;
        };
        this.router = express.Router();
        this.routing();
    }
    Router.URL_API_ARTICLES = '/api/public/articles';
    Router.URL_API_MOOVIES = '/api/public/moovies';
    Router.URL_API_PERSONS = '/api/public/persons';
    return Router;
}());
exports.Router = Router;
//# sourceMappingURL=router.js.map