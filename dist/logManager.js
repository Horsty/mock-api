"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var winston = require("winston");
var config_js_1 = require("./config.js");
var LogManager = /** @class */ (function () {
    function LogManager() {
        var transports = [];
        if (config_js_1.Config.getProperty("env") !== 'prod') {
            transports = [
                new winston.transports.Console({ format: winston.format.simple() })
            ];
        }
        else {
            transports = [
                new winston.transports.File({ filename: config_js_1.Config.getProperty("logsError"), level: 'error' }),
                new winston.transports.File({ filename: config_js_1.Config.getProperty("logsCombined") })
            ];
        }
        this.logger = winston.createLogger({
            level: 'info',
            format: winston.format.json(),
            transports: transports
        });
    }
    LogManager.getInstance = function () {
        return !LogManager.instance ? new LogManager() : LogManager.instance;
    };
    LogManager.info = function (message) {
        LogManager.getInstance().logger.info(message);
    };
    return LogManager;
}());
exports.LogManager = LogManager;
//# sourceMappingURL=logManager.js.map