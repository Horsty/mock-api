"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Config = /** @class */ (function () {
    function Config() {
        var env = process.argv[2] == undefined ? 'prod' : process.argv[2];
        var config = require('./environment/' + env + '.json');
        var logs = config["logs"];
        this.properties = {
            "env": env,
            "port": config["port"],
            "logsError": logs["error"],
            "logsCombined": logs["combined"]
        };
    }
    Config.getInstance = function () {
        return !Config.instance ? new Config() : Config.instance;
    };
    Config.getProperty = function (property) {
        return Config.getInstance().properties[property];
    };
    return Config;
}());
exports.Config = Config;
//# sourceMappingURL=config.js.map