"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var bodyParser = require("body-parser");
var express = require("express");
var router_js_1 = require("./router.js");
var config_js_1 = require("./config.js");
var logManager_1 = require("./logManager");
var Server = /** @class */ (function () {
    function Server() {
        var _this = this;
        this.routing = function () {
            //Autorisation des requêtes POST, GET ...
            _this.app.use(function (req, res, next) {
                res.header('Access-Control-Allow-Origin', '*');
                res.header('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE, OPTIONS');
                res.header('Access-Control-Allow-Headers', 'content-type');
                return next();
            });
            //Routage par défaut
            _this.app.use('/', _this.router.getRouter());
            //Si aucune route ne matche
            _this.app.use('/', function (req, res) {
                res.sendStatus(404);
            });
        };
        this.config = function () {
            _this.app.use(bodyParser.json());
            _this.app.use(bodyParser.urlencoded({ extended: true }));
        };
        this.run = function () {
            _this.app.listen(config_js_1.Config.getProperty("port"), function () {
                logManager_1.LogManager.info('Lancement du seveur');
                logManager_1.LogManager.info('On écoute sur le port ' + config_js_1.Config.getProperty("port"));
            });
        };
        this.app = express();
        this.router = new router_js_1.Router();
        this.config();
        this.routing();
        this.run();
    }
    return Server;
}());
var server = new Server();
//# sourceMappingURL=server.js.map