"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var controller_1 = require("./controller");
var ArticleIndex = /** @class */ (function () {
    function ArticleIndex() {
        var _this = this;
        this.routing = function () {
            var controller = new controller_1.ArticlesController();
            _this.router.get('/', function (req, res, next) { return controller.getArticles(req, res, next); });
        };
        this.getRouter = function () {
            return _this.router;
        };
        this.router = express.Router();
        this.routing();
    }
    return ArticleIndex;
}());
exports.ArticleIndex = ArticleIndex;
//# sourceMappingURL=index.js.map