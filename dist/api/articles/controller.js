"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ArticlesController = /** @class */ (function () {
    function ArticlesController() {
    }
    /**
     * Route : [HOST]/api/public/articles
     * Envoi un article formatté en json
     */
    ArticlesController.prototype.getArticles = function (req, res, next) {
        var response = require('../../environment/articles/articles.json');
        res.write(JSON.stringify(response));
        res.end();
    };
    return ArticlesController;
}());
exports.ArticlesController = ArticlesController;
//# sourceMappingURL=controller.js.map