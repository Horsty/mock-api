"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MooviesController = /** @class */ (function () {
    function MooviesController() {
    }
    /**
     * Route : [HOST]/api/public/moovie
     * Envoi la ressource moovie formatté en json
     */
    MooviesController.prototype.search = function (req, res, next) {
        var response = require('../../environment/moovies/search.json');
        res.write(JSON.stringify(response));
        res.end();
    };
    MooviesController.prototype.get = function (req, res, next) {
        var response = require('../../environment/moovies/get.json');
        res.write(JSON.stringify(response));
        res.end();
    };
    return MooviesController;
}());
exports.MooviesController = MooviesController;
//# sourceMappingURL=controller.js.map