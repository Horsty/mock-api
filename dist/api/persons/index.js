"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var controller_1 = require("./controller");
var PersonIndex = /** @class */ (function () {
    function PersonIndex() {
        var _this = this;
        this.routing = function () {
            var controller = new controller_1.PersonsController();
            _this.router.get('/', function (req, res, next) { return controller.search(req, res, next); });
            _this.router.get('/4', function (req, res, next) { return controller.get(req, res, next); });
        };
        this.getRouter = function () {
            return _this.router;
        };
        this.router = express.Router();
        this.routing();
    }
    return PersonIndex;
}());
exports.PersonIndex = PersonIndex;
//# sourceMappingURL=index.js.map