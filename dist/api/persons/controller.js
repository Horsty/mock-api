"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PersonsController = /** @class */ (function () {
    function PersonsController() {
    }
    /**
     * Route : [HOST]/api/public/persons
     * Envoi la ressource persons formatté en json
     */
    PersonsController.prototype.search = function (req, res, next) {
        var response = require('../../environment/persons/search.json');
        res.write(JSON.stringify(response));
        res.end();
    };
    PersonsController.prototype.get = function (req, res, next) {
        var response = require('../../environment/persons/get.json');
        res.write(JSON.stringify(response));
        res.end();
    };
    return PersonsController;
}());
exports.PersonsController = PersonsController;
//# sourceMappingURL=controller.js.map