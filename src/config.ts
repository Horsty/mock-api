export class Config {

  private static instance: Config;

  private properties: any;

  constructor() {
    
    const env: string = process.argv[2] == undefined ? 'prod' : process.argv[2];
    const config: any = require('./environment/' +  env + '.json');
    const logs: any = config["logs"];

    this.properties = {
      "env" : env,
      "port" : config["port"],
      "logsError" : logs["error"],
      "logsCombined" : logs["combined"]
    };
  }

  private static getInstance = () => {
    return !Config.instance ? new Config() : Config.instance;
  }

  public static getProperty = (property: string) => {
    return Config.getInstance().properties[property];
  }
}
