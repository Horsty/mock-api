import * as express from 'express';
import { ArticleIndex } from './api/articles';
import { MoovieIndex } from './api/moovies';
import { PersonIndex } from './api/persons';

export class Router {

  private router: express.Router;

  public static URL_API_ARTICLES = '/api/public/articles';
  public static URL_API_MOOVIES = '/api/public/moovies';
  public static URL_API_PERSONS = '/api/public/persons';


  constructor() {
    this.router = express.Router();
    this.routing();
  }

  private routing = () => {
    this.router.use(Router.URL_API_ARTICLES, new ArticleIndex().getRouter());
    this.router.use(Router.URL_API_MOOVIES, new MoovieIndex().getRouter());
    this.router.use(Router.URL_API_PERSONS, new PersonIndex().getRouter());
  }

  public getRouter = () => {
    return this.router;
  }
}
