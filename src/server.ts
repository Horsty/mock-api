import * as bodyParser from 'body-parser';
import * as express from 'express';
import {Router} from './router.js';
import {Config} from './config.js';
import {LogManager} from './logManager';

class Server {

  public app: express.Application;
  private router: Router;

  constructor() {
    this.app = express();
    this.router = new Router();
    this.config();
    this.routing();
    this.run();
  }

  

  public routing = () => {
    //Autorisation des requêtes POST, GET ...
    this.app.use((req: express.Request, res: express.Response, next: express.NextFunction) => 
    {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE, OPTIONS');
      res.header('Access-Control-Allow-Headers', 'content-type');
      return next();
    });

    //Routage par défaut
    this.app.use('/', this.router.getRouter());

    //Si aucune route ne matche
    this.app.use('/', (req: express.Request, res: express.Response) => {
      res.sendStatus(404);
    });
  }

  public config = () => {
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({extended: true}));
  }

  public run = () => {
    this.app.listen(Config.getProperty("port"), () => {
      LogManager.info('Lancement du seveur');
      LogManager.info('On écoute sur le port ' + Config.getProperty("port"));
    });
  }
}

const server: Server = new Server();
