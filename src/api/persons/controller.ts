import * as express from 'express';

export class PersonsController {

  constructor() {
  }

  /**
   * Route : [HOST]/api/public/persons
   * Envoi la ressource persons formatté en json
   */
  public search (req: express.Request, res: express.Response, next: express.NextFunction) {
    let response = require('../../environment/persons/persons.json');
    res.write(JSON.stringify(response));
    res.end();
  }

  public get (req: express.Request, res: express.Response, next: express.NextFunction){
    let response = require('../../environment/persons/person.json');
    res.write(JSON.stringify(response));
    res.end();
  }

}
