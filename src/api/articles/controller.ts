import * as express from 'express';

export class ArticlesController {

  constructor() {
  }

  /**
   * Route : [HOST]/api/public/articles
   * Envoi un article formatté en json
   */
  public getArticles (req: express.Request, res: express.Response, next: express.NextFunction) {
    let response = require('../../environment/articles/articles.json');
    res.write(JSON.stringify(response));
    res.end();
  }
}
