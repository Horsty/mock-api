import * as express from 'express';
import { ArticlesController } from './controller';

export class ArticleIndex {

  private router: express.Router;

  constructor(){
    this.router = express.Router();
    this.routing();
  }

  private routing = () => {
    const controller: ArticlesController = new ArticlesController();
    this.router.get('/', (req: express.Request, res: express.Response, next: express.NextFunction) => controller.getArticles(req, res, next));
  }

  public getRouter = () => {
    return this.router;
  }
}
