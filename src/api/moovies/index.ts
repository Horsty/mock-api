import * as express from 'express';
import { MooviesController } from './controller';

export class MoovieIndex {

  private router: express.Router;

  constructor(){
    this.router = express.Router();
    this.routing();
  }

  private routing = () => {
    const controller: MooviesController = new MooviesController();
    this.router.get('/', (req: express.Request, res: express.Response, next: express.NextFunction) => controller.search(req, res, next));
    this.router.get('/4', (req: express.Request, res: express.Response, next: express.NextFunction) => controller.get(req, res, next));
  }

  public getRouter = () => {
    return this.router;
  }
}
