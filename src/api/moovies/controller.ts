import * as express from 'express';

export class MooviesController {

  constructor() {
  }

  /**
   * Route : [HOST]/api/public/moovie
   * Envoi la ressource moovie formatté en json
   */
  public search (req: express.Request, res: express.Response, next: express.NextFunction) {
    let response = require('../../environment/moovies/moovies.json');
    res.write(JSON.stringify(response));
    res.end();
  }

  public get (req: express.Request, res: express.Response, next: express.NextFunction){
    let response = require('../../environment/moovies/moovie.json');
    res.write(JSON.stringify(response));
    res.end();
  }

}
