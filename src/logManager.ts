import * as winston from 'winston';
import {Config} from './config.js';

export class LogManager {

  private static instance: LogManager;

  private logger: winston.Logger;

  constructor(){

    let transports: winston.Transport[] = [];

    if (Config.getProperty("env") !== 'prod') {
      transports = [
        new winston.transports.Console({format: winston.format.simple()})
      ]
    } else {
      transports = [
        new winston.transports.File({ filename: Config.getProperty("logsError"), level: 'error' }),
        new winston.transports.File({ filename: Config.getProperty("logsCombined") })
      ]
    }

    this.logger = winston.createLogger({
      level: 'info',
      format: winston.format.json(),
      transports: transports
    });
  }

  private static getInstance = () => {
    return !LogManager.instance ? new LogManager() : LogManager.instance;
  }

  public static info = (message: string) => {
    LogManager.getInstance().logger.info(message);
  }
}
